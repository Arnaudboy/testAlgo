/*
Ecrivez une fonction add qui prend en paramètres 2 arguments
La fonction renvoie la somme des deux arguments
*/
function add(a, b){

}

/*
Ecrivez une fonction maxArray qui prend en paramètre un array(tableau)
Le tableau est un tableau de int uniquement
La fonction renvoie le plus grand nombre du tableau
*/
function maxArray(arr) {

}

/*
Ecrivez une fonction sumArray qui prend en paramètre un array
Le tableau est un tableau de int uniquement
La fonction renvoie la somme de tous les élèments du tableau
*/
function sumArray(arr) {

}


/*
Ecrivez une fonction reverseString qui prend en paramètre une chaîne de caractère
La fonction inverse l'ordre des caractère et renvoie la chaîne
*/
function reverseString(str) {
    
}

module.exports = {add, maxArray, sumArray, reverseString}