const assert = require('assert');
const {add, maxArray, sumArray, reverseString} = require('./exo_algo')

assert.strictEqual(add(1,2), 3)
assert.strictEqual(add(-3,2), -1)
assert.strictEqual(add(122,-2), 120)

assert.strictEqual(maxArray([1,2,3]), 3)
assert.strictEqual(maxArray([10,2,3]), 10)
assert.strictEqual(maxArray([1,2,3,12,23,78]), 78)

assert.strictEqual(sumArray([1,2,3]), 6)
assert.strictEqual(sumArray([11,2,3]), 16)
assert.strictEqual(sumArray([1,2,3,4,-5]), 5)

assert.strictEqual(reverseString("hello"), "olleh")
assert.strictEqual(reverseString("Hello World!"), "!dlroW olleH")
assert.strictEqual(reverseString("Success !"), "! sseccuS")