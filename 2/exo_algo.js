/*
Ecrivez une fonction rotateArray qui prend en paramètres un int n et un array
La fonction renvoie un array qui aura subit n rotation(s)
Une rotation est effectuée lorsque par exemple :
    [1,2,3] => [2,3,1]
Et pour 2 rotations :
    ['h', 'e', 'l', 'l', 'o'] => ['l', 'l', 'o', 'h', 'e']
*/
function rotateArray(n, arr) {
    
} 

/*
Ecrivez une fonction reverseString qui prend en paramètre une chaîne de caractère
La fonction inverse l'ordre des caractère et renvoie la chaîne
*/
function reverseString(str) {

}

/*
Ecrivez une fonction reverseWords qui prend en paramètre une string
La fonction renvoie une string dont l'ordre des caractères de chaque mots est inversé
L'ordre des mots est cependant conservé
    "Bonjour la team" => "roujnoB al meat"
*/
function reverseWords(str) {

}

/*
Ecrivez une fonction prof qui prend en paramètre un int
Vous devez définir combien de multiple de ce int sont nécessaire avant d'obtenir tous les chiffres de 0 à 9
Multiple         valeur        chiffres     comment
42*1              42            2,4 
42*2              84             8         4 déjà trouvé
42*3              126           1,6        2 déjà trouvé
42*4              168            -         all déjà trouvé
42*5              210            0         2,1 déjà trouvé
42*6              252            5         2 déjà trouvé
42*7              294            9         2,4 déjà trouvé
42*8              336            3         6 déjà trouvé 
42*9              378            7         3,8 déjà trouvé

pour 42 la fonction renvoie 9
*/
function prof(n) {
    
}

module.exports = {rotateArray, reverseString, reverseWords, prof}