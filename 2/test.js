const assert = require('assert');
const {rotateArray, reverseString, reverseWords, prof} = require('./exo_algo')

assert.strictEqual(rotateArray(1, [1,2,3]), [2,3,1])
assert.strictEqual(rotateArray(2, ['h', 'e', 'l', 'l', 'o']), ['l', 'l', 'o', 'h', 'e'])
assert.strictEqual(rotateArray(3,['y', 'e', 's']), ['y', 'e', 's'])

assert.strictEqual(reverseString("hello"), "olleh")
assert.strictEqual(reverseString("Hello World!"), "!dlroW olleH")
assert.strictEqual(reverseString("Success !"), "! sseccuS")

assert.strictEqual(reverseWords("Bonjour la team"), "ruojnoB al meat")
assert.strictEqual(reverseWords("This is an example!"), "sihT si na !elpmaxe")
assert.strictEqual(reverseWords("double  spaces"), "elbuod  secaps")

assert.strictEqual(prof(42), 9)
assert.strictEqual(prof(1), 10)
assert.strictEqual(prof(3), 10)
