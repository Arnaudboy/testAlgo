/*
Ecrivez une fonction prof qui prend en paramètre un int
Vous devez définir combien de multiple de ce int sont nécessaire avant d'obtenir tous les chiffres de 0 à 9
Multiple         valeur        chiffres     comment
42*1              42            2,4 
42*2              84             8         4 déjà trouvé
42*3              126           1,6        2 déjà trouvé
42*4              168            -         all déjà trouvé
42*5              210            0         2,1 déjà trouvé
42*6              252            5         2 déjà trouvé
42*7              294            9         2,4 déjà trouvé
42*8              336            3         6 déjà trouvé 
42*9              378            7         3,8 déjà trouvé

pour 42 la fonction renvoie 9
*/
function prof(n) {

}

/*
Ecrivez une fonction rebond prenant en paramètres 3 int/float : h, coeff et obs
Une balle est lâchée d'une hauteur h
A chaque rebond elle rebondit à une hauteur h*coeff (0 < coeff < 1)
Un observateur se trouve à une hauteur obs
Combien de fois l'observateur verra passer la balle devant lui
*/
function rebond(h, coeff, obs){

}

/*
un Hamming number -> https://en.wikipedia.org/wiki/Regular_number
2^i*3^j5^k (2puissancei fois 3puissancej fois 5puissance k)
Ecrivez une fonction ham qui prend en paramètre un int n
La fonction retourn le nième Hamming number (nombre de Hamming)
*/
function ham(n) {

}

module.exports = {prof, rebond, ham}