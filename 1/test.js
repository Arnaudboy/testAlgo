const assert = require('assert');
const {prof, rebond, ham} = require('./exo_algo')

assert.strictEqual(prof(42), 9)
assert.strictEqual(prof(1), 10)
assert.strictEqual(prof(3), 10)

assert.strictEqual(rebond(3, 0.66, 1.5), 3)
assert.strictEqual(rebond(10, 0.5, 1), 7)
assert.strictEqual(rebond(10, 0.1, 2.5), 1)

assert.strictEqual(ham(3), 3)
assert.strictEqual(ham(10), 12)
assert.strictEqual(ham(19), 32)
